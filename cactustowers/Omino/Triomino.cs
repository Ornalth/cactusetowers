public class Triomino {
	//Specials
	
    Block[] blocks;

	public static Triomino randomTriomino() {
		return new Triomino();
	}

    Triomino() {
    	blocks = new Block[3];
    	for (int i = 0; i < 3; i++) {
    		blocks[i] = Block.randomBlock();
    	}
    }

    public void changeOrientation() {
    	Block temp = blocks[0];
    	blocks[0] = blocks[2];
    	blocks[2] = blocks[1];
    	blocks[1] = temp;
    }

    public Block[] getBlocks() {
    	return blocks;
    }

    public Block this[int i]
    {
       get { return blocks[i]; }
    }
}