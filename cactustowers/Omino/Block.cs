using UnityEngine;

//regular block etc.
public class Block {
	PowerUp power;
	BlockType type;
	private static BlockType[] GENERATE_BLOCKS = new BlockType[] {
		BlockType.C1, 
		BlockType.A, 
		BlockType.C2, 
		BlockType.T, 
		BlockType.U,
		BlockType.S,
		BlockType.E
	};

	public Block() {
		type = BlockType.EMPTY;
	}

	public static Block randomBlock() {
		int blockType = Random.Range(0,GENERATE_BLOCKS.Length);
		return new Block(GENERATE_BLOCKS[blockType]);
	}

	public Block(BlockType b) {
		type = b;
		power = new PowerUpNone();
	}

	public Block(BlockType b, PowerUp p) {
		type = b;
		power = p;
	}
	public bool isEmpty() {
		return type == BlockType.EMPTY;
	}
    // conversion from BlockType to Block
    public static implicit operator Block(BlockType b)
    {
        return new Block(b);
    }

    public PowerUp getPowerUp() {
    	return power;
    }

    public BlockType getType() {
    	return type;
    }
}

public enum BlockType {
	EMPTY, 
	STONE, 
	MIDAS, 
	C1, 
	A, 
	C2, 
	T, 
	U,
	S,
	E,
	OUT_OF_BOUNDS,
}
