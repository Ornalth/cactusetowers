//regular block etc.
public abstract class PowerUp {

	// public PowerUp(BlockType t) {
		
	// }
	
	public virtual bool isNone() {
		return false;
	}


	public abstract void activate(Board board, Player target);
}