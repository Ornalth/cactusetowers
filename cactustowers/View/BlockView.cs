using UnityEngine;
using System.Collections;

public class BlockView : MonoBehaviour {

	Block block;
	SpriteRenderer blockSprite;
    public void Awake() {
        SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer sprite in sprites)
        {
            if (sprite.name.Equals("Image"))
            {
                blockSprite = sprite;
            }
        }
    }
    public void setBlock(Block block) {
        if (!this.block.Equals(block)) {
            this.block = block;
            updateView();
        }
    }

    private void updateView() {
        Sprite spr = ImageLibrary.getBlockImage(block);
        blockSprite.sprite = spr;
    }

    public Block getBlock() {
        return block;
    }
}

/*
http://coffeebreakcodes.com/sample-projects-unity/2d-tetris-game-tutorial-unity3d/
*/