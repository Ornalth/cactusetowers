
using UnityEngine;
using System.Collections;

public class MainVC : MonoBehaviour, BoardObserver{

	Board model;
	GameObject[,] boardView;
	BlockView[,] boardViewScripts;

	GameObject nextTriominoObject;
	TriominoView nextTriominoView;
	GameObject currentTriominoObject;
	TriominoView currentTriominoView;

	TypogenicText gameDetailsText;
	TypogenicText gameOverText;
	GameObject gameoverObject;

	void Awake() {
		model = new Board();
		
		int maxRows = model.getNumRows();
		int maxCols = model.getNumColumns();
		

		boardView = new GameObject[maxRows, maxCols];
		boardViewScripts = new BlockView[maxRows, maxCols];

		for (int row = 0; row < maxRows; row++) {
			for (int col = 0; col < maxCols; col++) {
				GameObject g = Instantiate (Resources.Load ("prefabs/Block")) as GameObject;
				boardView[row,col] = g;
				g.transform.name ="Block" + row + "," + col;
				boardViewScripts[row,col] = g.GetComponent<BlockView> ();
				g.transform.parent = transform;
				g.transform.position = new Vector3(col,row,0f);
			}
		}
		TypogenicText[] textComponents = gameObject.GetComponentsInChildren<TypogenicText>();
		foreach (TypogenicText comp in textComponents)
		{
			if (comp.name.Equals("details")) {
				gameDetailsText = comp;
			} else if (comp.name.Equals("gameover")) {
				gameOverText = comp;
				gameoverObject = transform.Find("GameOver").gameObject;
				ViewHelper.hide(gameoverObject);
			}
		}
	} 
    void Update() {
    	if (!model.isGameOver()) {
			if (Input.GetKeyDown(KeyCode.UpArrow)) {
				model.changeOrientation();
			}
	        else if (Input.GetKeyDown(KeyCode.DownArrow)) {
	        	model.moveDown();
	        }
	        else if (Input.GetKeyDown(KeyCode.LeftArrow)) {
	        	model.moveLeft();
	        }
	        else if (Input.GetKeyDown(KeyCode.RightArrow)) {
	        	model.moveRight();
	        }
	        else if (Input.GetKeyDown(KeyCode.Space)) {
	        	model.moveAllDown();
	        }
	        model.tick();
    	}
    }

    public void updateBoard() {
    	Block[,] blocks = model.getBoard();
    	for (int row = 0; row < model.getNumRows(); row++) {
			for (int col = 0; col < model.getNumColumns(); col++) {
				boardViewScripts[row,col].setBlock(blocks[row,col]);
			}
		}

		Triomino omino = model.getCurrentTriomino();
		Triomino next = model.getNextTriomino();
		Position currentOminoPosition = model.getCurrentTriominoPosition();
		
		nextTriominoView.setTriomino(next);
		currentTriominoView.setTriomino(omino);
		currentTriominoObject.transform.position = new Vector3(currentOminoPosition.col, currentOminoPosition.row, 0f);
    }

    public void gameIsOver() {
    	showGameOver();
    }

    private void showGameOver() {
    	ViewHelper.show(gameoverObject);
    }
}

/*
http://coffeebreakcodes.com/sample-projects-unity/2d-tetris-game-tutorial-unity3d/
*/