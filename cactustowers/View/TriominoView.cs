using UnityEngine;
using System.Collections;

public class TriominoView : MonoBehaviour {

	Triomino trio;
	BlockView block1;
    BlockView block2;
    BlockView block3;
    void Awake() {
        BlockView[] blocks = gameObject.GetComponentsInChildren<BlockView>();

        foreach (BlockView block in blocks)
        {
            if (block.name.Equals("Block1"))
            {
                block1 = block;
            } 
            else if (block.name.Equals("Block2"))
            {
                block2 = block;
            } 
            else if (block.name.Equals("Block3"))
            {
                block3 = block;
            }
        }
    }
    public void setTriomino(Triomino omino) {
        this.trio = omino;
        Block[] blocks = omino.getBlocks();
        block1.setBlock(blocks[0]);
        block2.setBlock(blocks[1]);
        block3.setBlock(blocks[2]);
    }
}

/*
http://coffeebreakcodes.com/sample-projects-unity/2d-tetris-game-tutorial-unity3d/
*/