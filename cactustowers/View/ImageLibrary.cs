using UnityEngine;
using System.Collections.Generic;
using System;

public class ImageLibrary {

	// Use this for initialization
	static string baseStr = "ProcessedCardImages/";
	static readonly string LOC = "Block/";

	static Dictionary <BlockType, Sprite> blockLibrary = null;
	
	static Dictionary<BlockType, string> blockImagesDict  = new Dictionary<BlockType, string>() {
			{BlockType.EMPTY, "empty"}, 
			{BlockType.STONE, "stone"}, 
			{BlockType.MIDAS, "midas"}, 
			{BlockType.C1, "c1"},
			{BlockType.A, "a"}, 
			{BlockType.C2, "c2"}, 
			{BlockType.T, "t"},
			{BlockType.U, "u"},
			{BlockType.S, "s"},
			{BlockType.E, "exclam"}
	};

	//TODO POwerups.
	public static Sprite getBlockImage(Block b)
	{
		BlockType t = b.getType();
		if (blockLibrary == null)
		{
			getBlockLibrary();
		}

		Sprite ans;
		if (!blockLibrary.TryGetValue(t, out ans))
		{
			Debug.Log("NO SPRITE NAMED" + t);
		}
		return ans;
	}

	private static void getBlockLibrary()
	{
		if (blockLibrary == null)
		{
			blockLibrary = new Dictionary<BlockType, Sprite>();
			
			foreach (BlockType type in blockImagesDict.Keys)
			{
				Sprite spr = Resources.Load <Sprite> (LOC + blockImagesDict[type]);
				blockLibrary[type] = spr;
			}
		}
	}
}
