public class Position {
   public int row;
   public int col;

   public Position(int row, int col) {
   		this.row = row;
   		this.col = col;
   }
   public Position modifyByDirection(Direction d) {
   		Position pos = d.getDirection();
   		return new Position(pos.row + row, pos.col + col);
   }
}