public sealed class Direction
{
    public static readonly Direction NORTH_EAST = new Direction(new Position(1, 1));
    public static readonly Direction NORTH = new Direction(new Position(0,1));
    public static readonly Direction NORTH_WEST = new Direction(new Position(-1, 1));
    public static readonly Direction WEST = new Direction(new Position(-1, 0));
    public static readonly Direction EAST = new Direction(new Position(1, 0));
    public static readonly Direction SOUTH_WEST = new Direction(new Position(-1, -1));
    public static readonly Direction SOUTH = new Direction(new Position(0, -1));
    public static readonly Direction SOUTH_EAST = new Direction(new Position(1, -1));
    private Position pos;

    private Direction(Position pos)
    {
        this.pos = pos;
    }

    public Position getDirection() {
    	return pos;
    }
}