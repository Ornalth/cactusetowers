using System.Collections.Generic;
using System;

public class Board {

	private static readonly int BOARD_COLUMNS = 6;
	private static readonly int BOARD_ROWS = 24;
	private static readonly int LAST_COLUMN = BOARD_COLUMNS -1;
	private static readonly int TOP_ROW = BOARD_ROWS - 1;
	private static readonly int DEAD_ROWS = 4;

	Block[,] board;
	// 0,0 is bottom left, BOARD_ROWS-1, BOARD_COLUMNS-1 is top right
	Triomino nextTriomino;
	Triomino currentTriomino;
	Position triominoPosition;
	int score;

	int gravityCounter;
	int gravity;
	int level;
	int numClearedBlocks;
	List<BoardObserver> observers;
	List<PowerUp> powerUps;
	bool gameOver = false;

	int cactusBonus;

	public String ToString() {
		return String.Format(@"
			Score {0}
			Level {1}
			numClearedBlocksTotal {2}
			Gravity {3}
			GravityCounter {4}
			",
            score, level, numClearedBlocks, gravity, gravityCounter);
	}

	public Board() {
		observers = new List<BoardObserver>();
		newGame();
	}

	public void newGame() {
		board = new Block[BOARD_ROWS, BOARD_COLUMNS];
		for (int row = 0; row < BOARD_ROWS; row++) {
			for (int col = 0; col < BOARD_COLUMNS; col++) {
				board[row,col] = new Block();
			}
		}
		
		currentTriomino = Triomino.randomTriomino();
		nextTriomino = Triomino.randomTriomino();
		triominoPosition = new Position(TOP_ROW, BOARD_COLUMNS/2);
		score = 0;
		gameOver = false;
		gravity = 20;
		level = 1;
		gravityCounter = 0;
		numClearedBlocks = 0;
		powerUps = new List<PowerUp>();
		cactusBonus = 0;
	}
	public Triomino getCurrentTriomino() {
		return currentTriomino;
	}
	public Triomino getNextTriomino() {
		return nextTriomino;
	}
	public Position getCurrentTriominoPosition() {
		return triominoPosition;
	}

	public Block[,] getBoard() {
		return board;
	}
	public int getNumRows() {
		return BOARD_ROWS;
	}
	public int getNumColumns() {
		return BOARD_COLUMNS;
	}
	public int getLevel() {
		return level;
	}

	public int getScore() {
		return score;
	}

	public int getNumClearedBlocks() {
		return numClearedBlocks;
	}


	public Block getBlock(int row, int col) {
		return board[row,col];
	}

	public bool isGameOver() {
		return gameOver;
	}


	public void subscribe(BoardObserver obs) {
		this.observers.Add(obs);
	}

	private void notifyObservers() {
		foreach (BoardObserver obs in observers) {
			obs.updateBoard();
		}
	}

	private void notifyGameOver() {
		foreach (BoardObserver obs in observers) {
			obs.gameIsOver();
		}
	}

	public void tick() {
		gravityCounter++;
		if (gravityCounter == gravity) {
			applyGravity();
		}
	}

	private void applyGravity() {
		gravityCounter = 0;
		moveDown();
	}

	public bool canFit(Position position) {
		for (int row = position.row; row < 3; row++) {
			if (hasBlock(row, position.col)) {
				return false;
			}
		}
		return true;
	}

	private void lockPiece() {

		for (int row = 0; row < 3; row++) {
			board[triominoPosition.row + row, triominoPosition.col] = currentTriomino[row];
		}

		int[] deleteTracker = new int[BOARD_ROWS];
		while (true) {
			HashSet<Position> deletedPositions = findDeleteMatches(deleteTracker);
			int deletedNumber = deletePositions(deletedPositions);
			if (deletedNumber == 0) {
				break;
			}
			numClearedBlocks += deletedNumber;
		}

		if (checkIsGameOver()) {
			gameOver = true;
		} else {
			currentTriomino = nextTriomino;
			// TODO power up ratios.
			nextTriomino = Triomino.randomTriomino();
			triominoPosition = new Position(TOP_ROW, BOARD_COLUMNS/2);
		}
	}

	private bool checkIsGameOver() {
		for (int row = BOARD_ROWS - DEAD_ROWS - 1; row < BOARD_ROWS; row++) {
			for (int col = 0; col < BOARD_COLUMNS; col++) {
				if (hasBlock(row, col)) {
					return true;
				}
			}
		} 
		return false;
	}

	private List<Position> getSameBlocksInARow(Position start, BlockType currentBlockType, Direction badDir, Direction goodDir) {
		//If same, we can ignore the other direction because we already found a larger set.
		List<Position> inARow = new List<Position>();
		inARow.Add(start);
		BlockType newType = getBlockTypeInDirection(start, badDir);
		if (newType != currentBlockType) {
			Position currentPosition = start;
			while (true) {
				currentPosition = currentPosition.modifyByDirection(goodDir);
				if (getBlockType(currentPosition) == currentBlockType) {
					inARow.Add(currentPosition);
				} else {
					break;
				}
			}
		}
		return inARow;
	}

	private bool checkCactusE(Position start, Direction d) {
		BlockType currentBlockType = getBlockType(start);
		if (currentBlockType == BlockType.C1 || currentBlockType == BlockType.C2) {

			// Only accept E, - hori 
			// NE,  SE, - diag
			// S - Vert
			if ( d == Direction.NORTH_EAST || d == Direction.EAST || d == Direction.SOUTH_EAST || d == Direction.SOUTH ) {

				//Exit early if we're moving to the right and not starting at col 0
				if ( (d == Direction.NORTH_EAST || d == Direction.EAST || d == Direction.SOUTH_EAST) && start.col != 0 ) {
					return false;
				}

				Position pos = start.modifyByDirection(d);
				if (getBlockType(pos) != BlockType.A) {
					return false;
				}

				pos = pos.modifyByDirection(d);
				BlockType otherC = getBlockType(pos);

				// Needs to be of C1 or C2 but not same as original.
				if (otherC != BlockType.C1 && otherC != BlockType.C2 || otherC == currentBlockType) {
					return false;
				}

				pos = pos.modifyByDirection(d);
				if (getBlockType(pos) != BlockType.T) {
					return false;
				}

				pos = pos.modifyByDirection(d);
				if (getBlockType(pos) != BlockType.U) {
					return false;
				}

				pos = pos.modifyByDirection(d);
				if (getBlockType(pos) != BlockType.S) {
					return false;
				}

				pos = pos.modifyByDirection(d);
				if (getBlockType(pos) != BlockType.E) {
					return false;
				}

				return true;
			}
		}

		return false;
	}

	// tracks how much of each deleted
	// index = track
	// 0 == HORI
	// 1 == DIAG
	// 2 == VERT
	private int HORI_TRACKER = 0;
	private int DIAG_TRACKER = 1;
	private int VERT_TRACKER = 2;
	private HashSet<Position> findDeleteMatches(int[] deleteTracker) {
		HashSet<Position> deletedPositions = new HashSet<Position>();
		

		//EXIT EARLY IF THE TOP MOST ROW IS EMPTY
		for (int row = 0; row < BOARD_ROWS; row++) {
			bool rowIsEmpty = true;
			for (int col = 0; col < BOARD_COLUMNS; col++) {
				BlockType currentBlockType = getBlockType(row, col);
				Position currentPosition = new Position(row,col);

				//CHECK BOTTOM 4 directions first, if they are true, we already attempted this match.
				if (currentBlockType != BlockType.EMPTY &&
					currentBlockType != BlockType.STONE) {
					rowIsEmpty = false;
					

					// NE
					List<Position> positionsInARow = getSameBlocksInARow(currentPosition, currentBlockType, Direction.SOUTH_WEST, Direction.NORTH_EAST);
					if (positionsInARow.Count >= 3) {
						deleteTracker[positionsInARow.Count]++;
						deletedPositions.UnionWith(positionsInARow);
					}

					//N
					positionsInARow = getSameBlocksInARow(currentPosition, currentBlockType, Direction.SOUTH, Direction.NORTH);
					if (positionsInARow.Count >= 3) {
						deleteTracker[positionsInARow.Count]++;
						deletedPositions.UnionWith(positionsInARow);
					}

					//NW
					positionsInARow = getSameBlocksInARow(currentPosition, currentBlockType, Direction.SOUTH_EAST, Direction.NORTH_WEST);
					if (positionsInARow.Count >= 3) {
						deleteTracker[positionsInARow.Count]++;
						deletedPositions.UnionWith(positionsInARow);
					}

					//E
					positionsInARow = getSameBlocksInARow(currentPosition, currentBlockType, Direction.WEST, Direction.EAST);
					if (positionsInARow.Count >= 3) {
						deleteTracker[positionsInARow.Count]++;
						deletedPositions.UnionWith(positionsInARow);
					}
				}
				
				// CHECK CACTUSE
				// TODO MERGE THESE INTO 1

				// VERT
				bool isCactusE = checkCactusE(currentPosition, Direction.SOUTH);
				if (isCactusE) {
					for (int i = 0; i < 6; i++) {
						int modcol = Direction.SOUTH.getDirection().col * i;
						int modrow = Direction.SOUTH.getDirection().row * i;
						deletedPositions.Add(new Position(currentPosition.row + modrow, currentPosition.col + modcol));
					}
					deleteTracker[VERT_TRACKER]++;
				}

				//DIAG
				isCactusE = checkCactusE(currentPosition, Direction.SOUTH_EAST);
				if (isCactusE) {
					for (int i = 0; i < 6; i++) {
						int modcol = Direction.SOUTH_EAST.getDirection().col * i;
						int modrow = Direction.SOUTH_EAST.getDirection().row * i;
						deletedPositions.Add(new Position(currentPosition.row + modrow, currentPosition.col + modcol));
					}
					deleteTracker[DIAG_TRACKER]++;
				}
				isCactusE = checkCactusE(currentPosition, Direction.NORTH_EAST);
				if (isCactusE) {
					for (int i = 0; i < 6; i++) {
						int modcol = Direction.NORTH_EAST.getDirection().col * i;
						int modrow = Direction.NORTH_EAST.getDirection().row * i;
						deletedPositions.Add(new Position(currentPosition.row + modrow, currentPosition.col + modcol));
					}
					deleteTracker[DIAG_TRACKER]++;
				}

				//HORI
				isCactusE = checkCactusE(currentPosition, Direction.EAST);
				if (isCactusE) {
					for (int i = 0; i < 6; i++) {
						int modcol = Direction.EAST.getDirection().col * i;
						int modrow = Direction.EAST.getDirection().row * i;
						deletedPositions.Add(new Position(currentPosition.row + modrow, currentPosition.col + modcol));
					}
					deleteTracker[HORI_TRACKER]++;
				}
			}

			if (rowIsEmpty) {
				break;
			}
		}

		return deletedPositions;
	}

	private int deletePositions(IEnumerable<Position> deleted) {
		int numDeleted = 0;

		//Count number of deleted per column
		//Index = column, value = num deleted inthta column
		int[] modifiedColumns = new int[BOARD_COLUMNS];

		// DELETE ALL APPROPRIATE POSITIONS
		foreach (Position pos in deleted) {
			int row = pos.row;
			int col = pos.col;

			Block deletedBlock = board[row,col];
			if (deletedBlock.getPowerUp().isNone()) {
				powerUps.Add(deletedBlock.getPowerUp());
			}

			board[row,col] = new Block(BlockType.EMPTY);
			modifiedColumns[col]++;
			numDeleted++;
		}

		// Per column, bubble down everything
		for (int col = 0; col < BOARD_COLUMNS; col++) {
			
			int numModified = modifiedColumns[col];
			if (numModified == 0) {
				continue;
			}

			//Starting from bottom of column, collect all non-blank values until we hit the number of blanks expected (modifiedColumns[col])
			List<Block> newColumn = new List<Block>();
			for (int row = 0; row < BOARD_ROWS && numModified > 0; row++) {

				BlockType currentBlockType = getBlockType(row, col);
				if (currentBlockType != BlockType.EMPTY) {
					modifiedColumns[col]--;
					newColumn.Add(board[row,col]);
				}
			}

			// Append same number of blanks at the top of the column
			for (int i = 0; i < modifiedColumns[col]; i++) {
				newColumn.Add(BlockType.EMPTY);
			}

			// Bubble everything down
			for (int row = 0; row < newColumn.Count; row++) {
				board[row,col] = newColumn[row];
			}
		}

		return numDeleted;
	}


	private Block[] newEmptyBlockRow() {
		Block[] blockRow = new Block[BOARD_COLUMNS];
		for (int i = 0; i < BOARD_COLUMNS; i++) {
			blockRow[i] = new Block();
		}
		return blockRow;
	}

	// Key Actions
	public void changeOrientation() {
		currentTriomino.changeOrientation();
		// if (success) {
			notifyObservers();
		// }
	}

	public void moveLeft() {
		Position newTritriominoPosition = triominoPosition.modifyByDirection(Direction.WEST);
		if (canFit(newTritriominoPosition)) {
			triominoPosition = newTritriominoPosition;
			notifyObservers();
		}
	}

	public void moveRight() {
		Position newTritriominoPosition = triominoPosition.modifyByDirection(Direction.EAST);
		if (canFit(newTritriominoPosition)) {
			triominoPosition = newTritriominoPosition;
			notifyObservers();
		}
	}

	public void moveDown() {
		Position newTritriominoPosition = triominoPosition.modifyByDirection(Direction.SOUTH);
		if (canFit(newTritriominoPosition)) {
			triominoPosition = newTritriominoPosition;
		} else {
			lockPiece();
		}

		if (isGameOver()) {
			notifyGameOver();
		} else {
			notifyObservers();
		}
		
	}

	public void moveAllDown() {
		while (true) {
			Position newTriominoPosition = triominoPosition.modifyByDirection(Direction.SOUTH);
			if (canFit(newTriominoPosition)) {
				triominoPosition = newTriominoPosition;
			} else {
				lockPiece();
				break;
			}
		}
		if (isGameOver()) {
			notifyGameOver();
		} else {
			notifyObservers();
		}
	}

	//TODO
	public void usePower() {
		if (powerUps.Count == 0) {
			return;
		}
	}

	public void usePower(int target) {
		if (powerUps.Count == 0) {
			return;
		}
	}

	// Returns true for invalid positions
	bool isSurrounded(Position p) {
		foreach (Direction d in Enum.GetValues(typeof(Direction))) {
			if (!hasBlockInDirection(p, d)) {
				return false;
			}
		}
		return true;
	}

	// Returns true for invalid positions
	bool hasBlockInDirection(Position p, Direction d) {
		Position pos = p.modifyByDirection(d);
		return hasBlock(pos.row, pos.col);
	}


	private BlockType getBlockTypeInDirection (Position p, Direction d) {
		Position pos = p.modifyByDirection(d);
		return getBlockType(pos.row, pos.col);
	}

	private BlockType getBlockType (int row, int col) {
		if (row < 0 || row >= BOARD_ROWS) {
			return BlockType.OUT_OF_BOUNDS;
		}
		if (col < 0 || col >= BOARD_COLUMNS) {
			return BlockType.OUT_OF_BOUNDS;
		}

		return board[row,col].getType();
	}

	private BlockType getBlockType (Position pos) {
		return getBlockType(pos.row, pos.col);
	}

	// Returns true for invalid positions
	bool hasBlock(int row, int col) {
		//Out Of Bounds
		if (row < 0 || row >= BOARD_ROWS) {
			return true;
		}
		if (col < 0 || col >= BOARD_COLUMNS) {
			return true;
		}

		return board[row,col].getType() != BlockType.EMPTY;
	}

}