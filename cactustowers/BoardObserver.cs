public interface BoardObserver {
	void updateBoard();
	void gameIsOver();
	
}