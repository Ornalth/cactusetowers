using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public static class ViewHelper {
	static float EPSILION = 0.0001f;

	public static void hide(GameObject view)
	{
		view.SetActive (false);

	}
	
	public static void show(GameObject view)
	{
		view.SetActive (true);
	}


	public static Texture2D convertSpriteToTexture(Sprite sprite)
	{
		Texture2D croppedTexture = new Texture2D( (int)sprite.rect.width, (int)sprite.rect.height );
 
		Color[] pixels = sprite.texture.GetPixels( (int)sprite.textureRect.x,
		(int)sprite.textureRect.y,
		(int)sprite.textureRect.width,
		(int)sprite.textureRect.height );
		 
		croppedTexture.SetPixels( pixels );
		croppedTexture.Apply();

		return croppedTexture;
 
	}

	public static bool isColorEqual(Color c1, Color c2)
	{
		if((int)(c1.r * 1000) == (int)(c2.r * 1000) &&
			(int)(c1.g * 1000) == (int)(c2.g * 1000) &&
			(int)(c1.b * 1000) == (int)(c2.b * 1000))
		{
			return true;
		}
		return false;
	}

	public static bool V3Equals(Vector3 v1, Vector3 v2)
	{
		return Vector3.SqrMagnitude(v1 - v2) < EPSILION;
	}

	public static int getScreenWidth()
	{
		return Screen.width;
	}

	public static int getScreenHeight()
	{
		return Screen.height;
	}

	public static void setTextColor(TypogenicText text, Color c)
	{
		text.ColorTopLeft = c;
		text.ColorTopRight = c;
		text.ColorBottomLeft = c;
		text.ColorBottomRight = c;
	}
}
